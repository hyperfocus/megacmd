#!/usr/bin/env bash

cd ~

wget https://mega.nz/linux/MEGAsync/Debian_9.0/amd64/megacmd-Debian_9.0_amd64.deb

apt install -y libmms0 libtinyxml2-4 libc-ares2 libcrypto++6 libmediainfo0v5 libzen0v5

echo path-include /usr/share/doc/megacmd/* > /etc/dpkg/dpkg.cfg.d/docker

apt install -y ./megacmd-Debian_9.0_amd64.deb